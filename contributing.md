# Contributing

Testing (with `bash`):

```shell
SHELL="/bin/bash" asdf plugin test lsd ./.git --asdf-plugin-gitref "$(git rev-parse --abbrev-ref HEAD)" -- "lsd --version"
```

Tests are automatically run in GitLab CI on push and merge request.
