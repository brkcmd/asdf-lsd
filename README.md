<div align="center">

# asdf-lsd ![Build Status](https://gitlab.com/brkcmd/asdf-lsd/badges/trunk/pipeline.svg)

[lsd](https://github.com/lsd-rs/lsd) plugin for the [asdf version manager](https://asdf-vm.com).

</div>

# Contents

- [Dependencies](#dependencies)
- [Install](#install)
- [Contributing](#contributing)
- [License](#license)

# Dependencies

- `bash`, `curl`, `tar`: generic POSIX utilities.

# Install

Plugin:

```shell
asdf plugin add https://gitlab.com/brkcmd/asdf-lsd.git
```

`lsd`:

```shell
# Show all installable versions
asdf list-all lsd

# Install specific version
asdf install lsd latest

# Set a version globally (on your ~/.tool-versions file)
asdf global lsd latest

# Now lsd commands are available
lsd
```

Check [asdf](https://github.com/asdf-vm/asdf) readme for more instructions on how to
install & manage versions.

# Contributing

Contributions of any kind welcome! See the [contributing guide](contributing.md).

[Thanks goes to these contributors](https://gitlab.com/brkcmd/asdf-lsd/-/graphs/trunk)!

# License

See [LICENSE](LICENSE) © [brkcmd](https://gitlab.com/brkcmd/)
